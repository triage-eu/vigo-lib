# Vigo Detector

Python package to read Vigo detector.

## Prerequisites

If using Linux, an udev rule need to be added to use the USB device. For example, open or create the document /etc/udev/rules.d/99-com.rules:

```shell
sudo nano /etc/udev/rules.d/99-com.rules
```

and add the following lines:

```shell
SUBSYSTEM=="usb", ATTR{idVendor}=="0403", ATTR{idProduct}=="6010", MODE="0666"
SUBSYSTEM=="usb", DRIVER=="ftdi_sio", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6010", ATTRS{product}=="TRIAGE", RUN+="/bin/sh -c 'echo $kernel > /sys/bus/usb/drivers/ftdi_sio/unbind'"
```

Adapt **0403** and **6010** to match `idVendor` and `idProduct` accordingly. They can be found using command `lsusb`.

## Installation

The `vigo` package can be installed with `pip` after cloning the repository. 

```shell
git clone https://gitlab.csem.local/div-e/project/triage/vigo-lib.git
cd vigo-lib

# Optional but strongly recommended.
virtualenv venv
source venv/bin/activate  # or venv\Scripts\activate on Windows.

pip install --editable .
```

## Usage

The package provides a simple interface to read Vigo detector measurements.
