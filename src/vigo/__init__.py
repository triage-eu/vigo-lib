#!/usr/bin/env python3

#from pyftdi.ftdi import Ftdi
import ftd2xx
import time
import datetime
import numpy as np
from configparser import ConfigParser
import sys
import tomlkit
import struct
from pathlib import Path

COEFF = 76.3 / 1000
OFFSET_TRIG = 1497.5
OFFSET_SIG = 1498.5
OFFSET_REF = 1498.5

F_SAMPLING = 640000

CONFIG_FILE = "/home/triage/Documents/python_scripts/pre_processing/init_processing_files/triage.toml"

class Vigo:
    dev = None
    frame_size = 2048

    def __init__(self):
        devs = ftd2xx.listDevices()
        if devs != None:
            self.dev = ftd2xx.openEx(bytes("TRIAGE A", "ascii"), 2)
            self.dev2 = ftd2xx.openEx(bytes("TRIAGE B", "ascii"), 2)
            if (self.dev2):
                self.dev2.setBitMode(0xff, 0x00)
                #print("Disabled JTAG")
            time.sleep(0.1)
            self.dev.setTimeouts(100, 100)
            time.sleep(0.1)
            self.dev.setUSBParameters(0x10000, 0x10000)
            time.sleep(0.1)
            self.dev.setLatencyTimer(1)
            time.sleep(0.1)
            self.dev.setFlowControl(ftd2xx.defines.FLOW_RTS_CTS, 0, 0)
            time.sleep(0.1)
            self.dev.purge(ftd2xx.defines.PURGE_RX)
            time.sleep(0.1)
            self.dev.purge(ftd2xx.defines.PURGE_TX)
            time.sleep(0.1)
        else:
            print("Device not found!")
            self.close()

    def __del__(self):
        if self.dev:
            self.close()

    def close(self):
        if self.dev:
            try:
                self.dev.setBitMode(0xff, 0x00)
                self.dev.close()
            except:
                pass

    def setDAC(self, dac, value):
        tx = [0x01, dac]
        val = struct.pack(">H", value)
        tx.extend(val)
        tx.extend([0x00, 0xfe])
        ret = self.dev.write(bytes(tx))
        if ret:
            return 0
        else:
            print(f"Error setting DAC[{dac}]")
            return -1

    def setADCDelay(self, adc1_delay, adc2_delay):
        tx = [0x08]
        val = struct.pack(">H", adc1_delay)
        tx.extend(val)
        val = struct.pack(">H", adc2_delay)
        tx.extend(val)
        tx.extend([0xf7])
        ret = self.dev.write(bytes(tx))
        if ret:
            return 0
        else:
            print(f"Error setting ADC delay[{dac}]")
            return -1

    def configDAC(self):
        config_file = CONFIG_FILE
        self.config = None
        try:
            self.config = tomlkit.loads(Path(config_file).read_text())
        except:
            print("Failed to load config file, won't set delay and DAC values when sampling!")

        if (self.config):
            print("Setting delay...")
            self.setADCDelay(self.config['ADC']['adc1_delay'], self.config['ADC']['adc2_delay'])
            print("Setting DACs...")
            self.setDAC(0, self.convertDAC(0, self.config['DAC']['dac0']))
            self.setDAC(1, self.convertDAC(1, self.config['DAC']['dac1']))
            self.setDAC(2, self.convertDAC(2, self.config['DAC']['dac2']))
            self.setDAC(3, self.convertDAC(3, self.config['DAC']['dac3']))

    def startSampling(self, count):
        print(f"Recording {count} samples.")
        tx = [0x02]
        val = bytes(struct.pack(">I", count))
        tx.extend(val)
        tx.extend([0xfd])
        self.dev.write(bytes(tx))

        #read progress
        tx = [0x04, 0x00, 0x00, 0x00, 0x00, 0xfb]
        self.dev.write(bytes(tx))
        previous = 0

        while True:
            status = self.dev.getStatus()
            rx_available = status[0]
            if (rx_available >= 8):
                data = self.dev.read(8)
                written = struct.unpack("<I", data[0:4])[0]
                total = struct.unpack("<I", data[4:8])[0]
                diff = written - previous
                previous = written
                if (total == written):
                    break
                self.dev.write(bytes(tx))

    def downloadSamples(self, count):
        print(f"Downloading {count} samples.")
        data = bytearray()  # Empty list
        read_samples = 0
        while read_samples < (count * 3 * 2):
            cmd = [0x05]
            cmd.extend(bytes(struct.pack(">I", read_samples)))
            cmd.append(0xfa)
            self.dev.write(bytes(cmd))
            cnt = 0

            while cnt < self.frame_size:
                status = self.dev.getStatus()
                rx_available = status[0]
                retry = 0
                if rx_available >= self.frame_size:
                    d = self.dev.read(self.frame_size)
                    cnt = self.frame_size
                    data = data + d
            read_samples = read_samples + self.frame_size
        return data

    def convertDAC(self, channel, value):
        #DAC0 and DAC1 are unipolar
        if channel == 0 or channel == 1:
            if value < 0 or value > 2000:
                print("Wrong value, must be between 0 and 2000")
                return -1
            value_raw = int((value * 65536) / 4500)
            if (value_raw > 65535):
                value_raw = 65535
            return value_raw
        #DAC2(ref) and DAC3(sig) are bipolar
        elif channel == 2 or channel == 3:
            if value < -2500 or value > 2500:
                print("Wrong value, must be between -2500 and 2500")
                return -1
            #shift to positive
            value_raw = (value + 2500) / 2
            value_raw = int((value_raw * 65536) / 2500)
            if (value_raw > 65535):
                value_raw = 65535
            return value_raw
        else:
            print("Wrong DAC channel!")
            return -1

    def getCelibrationSamples(self):
        samples = []
        ref = []
        sig = []
        count = 1024
        #get 1024 samples
        self.startSampling(6 * count, 0)
        #download them
        read_samples = 0
        while read_samples < (count * 3 * 2):
            cmd = [0x05]
            cmd.extend(bytes(struct.pack(">I", read_samples)))
            cmd.append(0xfa)
            self.dev.write(bytes(cmd))
            cnt = 0

            while cnt < self.frame_size:
                status = self.dev.getStatus()
                rx_available = status[0]
                retry = 0
                if rx_available >= self.frame_size:
                    data = self.dev.read(self.frame_size)
                    cnt = self.frame_size
                    samples.extend(data)
            read_samples = read_samples + self.frame_size

        for i in range(0, count * 6, 6):
            data = samples[i:i+6]
            data = struct.unpack("<HHH", bytes(data))
            ref.append(data[1])
            sig.append(data[2])

        return [np.average(ref) * COEFF - OFFSET_REF, np.average(sig) * COEFF - OFFSET_SIG]

    def calibrationPerformTest(self, dac2, dac3):
        self.setDAC(2, dac2)
        self.setDAC(3, dac3)
        time.sleep(0.5)
        [ref, sig] = self.getCelibrationSamples()
        return [ref, sig]

    def calibrationSign(self, ref, sig, ref_0, sig_0, offset):
        print(f"ref: {ref} mV, sig: {sig} mV")
        ref_diff = ref - (ref_0 - offset)
        sig_diff = sig - (sig_0 + offset)
        print(f"ref_diff: {ref_diff} mV, sig_diff: {sig_diff} mV")
        [ref_sign, sig_sign] = [ref_diff > 0, sig_diff > 0]
        print(f"ref_sign: {ref_sign}, sig_sign: {sig_sign}")
        return [ref_sign, sig_sign]

    def calibrateOffsets(self, args):
        #step 1 - individually set DACs to zero voltage +/- offset
        ref_0 = int(args.calibrate[0])
        sig_0 = int(args.calibrate[1])
        offset = int(args.calibrate[2])
        save = int(args.calibrate[3])
        dac2 = 32768
        dac3 = 32768
        step_dac2 = 5000
        step_dac3 = 5000
        [ref, sig] = self.calibrationPerformTest(dac2, dac3)
        [ref_sign, sig_sign] = self.calibrationSign(ref, sig, ref_0, sig_0, offset)
        print(f"DAC2: {dac2}, DAC3: {dac3}, step_dac2: {step_dac2}, step_dac3: {step_dac3}\r\n#################")
        while True:
            if (ref_sign):
                dac2 = dac2 + step_dac2
                if (dac2 > 65535):
                    dac2 = 65535
            else:
                dac2 = dac2 - step_dac2
                if (dac2 < 0):
                    dac2 = 0
            if (sig_sign):
                dac3 = dac3 + step_dac3
                if (dac3 > 65535):
                    dac3 = 65535
            else:
                dac3 = dac3 - step_dac3
                if (dac3 < 0):
                    dac3 = 0
            [ref, sig] = self.calibrationPerformTest(dac2, dac3)
            [ref_sign_new, sig_sign_new] = self.calibrationSign(ref, sig, ref_0, sig_0, offset)
            if (ref_sign != ref_sign_new):
                ref_sign = ref_sign_new
                step_dac2 = int(math.ceil(step_dac2 / 2))
            if (sig_sign != sig_sign_new):
                sig_sign = sig_sign_new
                step_dac3 = int(math.ceil(step_dac3 / 2))
            print(f"DAC2: {dac2}, DAC3: {dac3}, step_dac2: {step_dac2}, step_dac3: {step_dac3}\r\n#################")
            if (step_dac2 == 1 and step_dac3 == 1):
                print(f"Done, DAC settings -  DAC2: {int((dac2 / 65536) * 5000 - 2500)} mV, DAC3: {int((dac3 / 65536) * 5000 - 2500)} mV")
                #return [dac2, dac3]
                return [int((dac2 / 65536) * 5000 - 2500), int((dac3 / 65536) * 5000 - 2500)]

    def tuneOffsets(self, dac2, dac3):
        [ref, sig] = self.calibrationPerformTest(dac2, dac3)
        diff = ref - sig
        print(f"ref({ref}) - sig({sig}) = {diff}")

    def calibrate(self):
        print("Setting gain DACs...")
        config_file = CONFIG_FILE
        self.setDAC(0, self.convertDAC(0, config['DAC']['dac0']))
        self.setDAC(1, self.convertDAC(1, config['DAC']['dac1']))
        [dac2, dac3] = self.calibrateOffsets(args)
        #self.tuneOffsets(dac2, dac3)
        config['DAC']['dac2'] = dac2
        config['DAC']['dac3'] = dac3
        with Path(config_file).open("w") as f:
            f.write(tomlkit.dumps(config))

    def parseData(self, samples, count):
        (t, ch1, ch2, ch3) = ([], [], [], [])

        #samples = samples[0]
        samples_count = int(len(samples) / 6) * 6
        skip_cnt_adc1 = 0
        skip_cnt_adc2 = 0

        for i in range(0, samples_count, 6):
            data = samples[i:i+6]
            data = struct.unpack("<HHH", data)

            #detect delay, samples that come from the delayed ADC are set to 0xffff
            #count how many, don't append then, and cut that ammount from the non delayed ADC'
            if (data[0] == 0xffff and data[1] == 0xffff):
                skip_cnt_adc1 = skip_cnt_adc1 + 1
            else:
                ch1.append(data[0])
                ch2.append(data[1])
            if (data[2] == 0xffff):
                skip_cnt_adc2 = skip_cnt_adc2 + 1
            else:
                ch3.append(data[2])

        if (skip_cnt_adc1 > 0 and skip_cnt_adc2 > 0):
            print("Both ADCs were delayed, this is not supported!")
            exit(-1)

        if (skip_cnt_adc1 > 0):
            ch3 = ch3[0:len(ch3) - skip_cnt_adc1]

        if (skip_cnt_adc2 > 0):
            ch1 = ch1[0:len(ch1) - skip_cnt_adc2]
            ch2 = ch2[0:len(ch2) - skip_cnt_adc2]

        #create time vector
        time = np.arange(0, len(ch1) / F_SAMPLING, 1/F_SAMPLING)
        #convert to Volts
        ch1 = np.multiply(ch1, COEFF/1000) - OFFSET_TRIG/1000
        ch2 = np.multiply(ch2, COEFF/1000) - OFFSET_REF/1000
        ch3 = np.multiply(ch3, COEFF/1000) - OFFSET_SIG/1000

        ch1 = ch1[0:count]
        ch2 = ch2[0:count]
        ch3 = ch3[0:count]
        time = time[0:count]

        return time, ch1, ch2, ch3

    def getSamples(self, count):
        self.startSampling(count)
        data = self.downloadSamples(count)
        t, ch1, ch2, ch3 = self.parseData(data, count)
        #res = [0] * 4 * len(t)
        #res[0::4] = t
        #res[1::4] = ch1
        #res[2::4] = ch2
        #res[3::4] = ch3
        res = [0] * 3 * len(t)
        res[0::3] = ch1
        res[1::3] = ch2
        res[2::3] = ch3
        res = np.array(res)
        res = np.ndarray.tobytes(res)
        data_len = len(t)
        return res, data_len

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    # Instantiate device
    vigo = Vigo()
    (i, N) = (0, 1)
    nsamples = np.zeros(N)

    f_csv = open("data/nsamples.csv", "w")

    while i < N:
        count = 500 #3200000
        if (count > 5500000):
            print("Maximum number of samples is 550000!")
            exit(-1)

        vigo.configDAC()
        vigo.startSampling(count)
        data = vigo.downloadSamples(count)

        nsamples[i] = len(data) / 8
        if nsamples[i] > 0:
            # Save nsamples
            f_csv.write(f'{nsamples[i]}\n')
            # Save data
            f_csvi = open(f'data/sample{i}.csv', "w")
            f_csvi.write("Sample_no;t[s];Balanced[mV];Laser[mV]\n")
            t, ch1, ch2, ch3 = vigo.parseData(data, count)
            print("len t ", len(t))
            for j in range(len(t)):
                f_csvi.write(f'{j};{t[j]:.10f};{ch1[j]};{ch2[j]}\n')

            #maxlim = np.max(ch2)
            #minlim = np.min(ch2)
            #ch2 = (ch2 - (maxlim+minlim)/2)*2/(maxlim-minlim)
            #ch2_diff = np.diff(ch2)/(2*3.1416*1e4*t[1])
            #plt.plot(t, ch2, 'b', t[:-1], ch2_diff, 'r')
            #plt.plot(t, ch1, 'b', t, ch2, 'r', t, ch3, 'g')
            plt.plot( ch1, 'b', ch2, 'r', ch3, 'g')
            plt.show()
            plt.savefig(f'data/sample{i}.png')
            plt.xlim(0, 0.0001)
            plt.savefig(f'data/sample{i}_zoom.png')
            plt.close()
            i += 1
        else:
            print('No data received')

    f_csv.close()
    vigo.close()
